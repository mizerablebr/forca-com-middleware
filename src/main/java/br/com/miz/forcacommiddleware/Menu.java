package br.com.miz.forcacommiddleware;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    private List<String> options;

    public Menu() {
        this.options = new ArrayList<>();
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public Menu addOption(String option) {
        this.options.add(String.format("%d - %s", options.size() + 1, option));
        return this;
    }
    public Menu addOption(int numero, String option) {
        this.options.add(String.format("%d - %s", numero, option));
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("##### Menu ####\n");
        options.forEach(op -> sb.append(String.format("%s\n", op)));
        sb.append("Digite uma das opções: ");
        return sb.toString();
    }
}
