package br.com.miz.forcacommiddleware;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import picocli.CommandLine;

@SpringBootApplication
public class ForcaComMiddlewareApplication implements CommandLineRunner, ExitCodeGenerator {

    private CommandLine.IFactory factory;
    private ForcaCommand forcaCommand;
    private int exitCode;

    public ForcaComMiddlewareApplication(CommandLine.IFactory factory, ForcaCommand forcaCommand) {
        this.factory = factory;
        this.forcaCommand = forcaCommand;
    }

    @Override
    public void run(String... args) throws Exception {
        // let picocli parse command line args and run the business logic
        exitCode = new CommandLine(forcaCommand, factory).execute(args);
    }

    @Override
    public int getExitCode() {
        return exitCode;
    }

    public static void main(String[] args) {
        // let Spring instantiate and inject dependencies
        System.exit(SpringApplication.exit(SpringApplication.run(ForcaComMiddlewareApplication.class, args)));
    }
}
