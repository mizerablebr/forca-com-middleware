package br.com.miz.forcacommiddleware.domain;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Game {
    private Map<String, List<String>> words;
    private List<Integer> roulette = Arrays.asList(0, 50, 100, 500, 1000);
    private LinkedList<Player> players;
    private List<String> characters = new ArrayList<>();
    private HashMap<Player, Integer> playerScore = new HashMap<>();
    private String electedWord = "";
    private int currentScore;

    public Game() {
        this.words = new HashMap<>();
        this.players = new LinkedList<>();
        setWords(Map.of("Frutas", Arrays.asList("Banana", "Morango", "Jaca")));
    }

    public Map<String, List<String>> getWords() {
        return words;
    }

    public void setWords(Map<String, List<String>> words) {
        this.words = words;
        electWords();
    }

    public Queue<Player> getPlayers() {
        return players;
    }

    public Game addPlayer(Player player) {
        this.players.add(player);
        playerScore.put(player, 0);
        return this;
    }

    public Game addPlayer(String nome) {
        Player player = new Player(nome);
        return addPlayer(player);
    }

    public List<String> getCharacters() {
        return characters;
    }

    public boolean addCharacters(String character) {
        this.characters.add(character.toUpperCase());
        boolean scored = getCurrentWords().stream()
                .anyMatch(w -> w.toUpperCase().contains(character.toUpperCase()));
        // Atribui pontuação ao jogador
        if (scored)
            playerScore.compute(getCurrentPlayer(), (k, v) -> v + currentScore);
        return scored;
    }

    /**
     * Seleciona pseudoaleatoriamente um conjunto de palavras com sua respetiva dica
     * As palavras secretas estão organizadas num  Map<String, List<String>> {@link Game#words}
     * onde o primeiro parâmetro é a descrição do conjunto de palavras, ou seja, sua dica
     * @return A palavra escolhida e atualiza o parâmetro {@link Game#electedWord} com ela.
     */
    public String electWords() {
        if (electedWord.equals("")) {
            List<String> wordsGroup = new ArrayList<>(words.keySet());
            Collections.shuffle(wordsGroup);
            electedWord = wordsGroup.get(0);
        }
        return electedWord;
    }

    /**
     * Oculta os caracteres das palavras secretas e retorna uma string revelando as letras
     * a depender das letras que já foram escolhidas em {@link Game#addCharacters(String)}
     * @return
     */
    public String wordsToPrint() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("<%s>", electedWord));
        sb.append(String.format("\n[%s]", String.join(" ", getCharacters())));
        getCurrentWords().forEach(w -> {
            // Remove letras que não esteja em characters
            String regex = ".";
            if (characters.size() > 0)
                regex = Stream.concat(characters.stream(), characters.stream()
                                            .map(String::toLowerCase))
                            .collect(Collectors.
                                    joining("|", "[^", "]"));
            String wToPrint = w.replaceAll(regex, "_");
            sb.append("\n").append(wToPrint);
        });
        return sb.toString();
    }

    /**
     * Retorna o primeiro jogador da fila e o coloca no fim da fila
     * @return {@link Player} que está em primeiro lugar na fila
     */
    public Player getNextPlayer() {
        Player player = players.poll();
        players.addLast(player);
        return player;
    }

    public Player getCurrentPlayer() {
        return players.peek();
    }

    /**
     * Gira a roleta e devolve o valor dos pontos da rodada atual. Caso o valor seja 0,
     * considera que o jogador perdeu a vez e executa o método {@link Game#getNextPlayer()}
     * @return Pontuação gerada na roleta
     */
    public int generateScore() {
        Collections.shuffle(this.roulette);
        // Se 0, perde a rodada
        if (this.roulette.get(0) == 0)
            getNextPlayer();
        return generateScore(this.roulette.get(0));
    }
    public int generateScore(int customScore) {
        this.currentScore = customScore;
        return this.currentScore;
    }

    /**
     * Verifica se a tentativa de descobrir as palavras ocultas foi acertada
     * @param guessedWords tentativa de adivinhar as palavras
     * @return True se acertou as 3 palavras.
     */
    public boolean guessedWordsCorrect(List<String> guessedWords) {
        guessedWords = guessedWords.stream().map(String::toUpperCase).collect(Collectors.toList());
        return getCurrentWords().stream()
                .map(String::toUpperCase)
                .allMatch(guessedWords::contains);
    }

    private List<String> getCurrentWords() {
        return words.get(electedWord);
    }

    /**
     * Revela se o jogo ainda aceita jogadas, ou seja, se do total de 7 jogadas
     * ainda restam algumas para poder fazer a tentativa de acertar
     * @return True se já escolheu 7 letras
     */
    public boolean canIGuess() {
        return getCharacters().size() >= 7;
    }

    public void spinRoulette() {
        System.out.println();
        for (int i = 0; i < 10; i++) {
            System.out.print("-\r");
            try {
                Thread.sleep(50 + (i * 10));
                System.out.print("\\\r");
                Thread.sleep(50 + (i * 10));
                System.out.print("|\r");
                Thread.sleep(50 + (i * 10));
                System.out.print("/\r");
                Thread.sleep(50 + (i * 10));
                System.out.print("\r");
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
    }

    public int getPlayerScore(Player player) {
        return playerScore.get(player);
    }
}
