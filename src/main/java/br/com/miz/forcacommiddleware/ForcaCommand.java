package br.com.miz.forcacommiddleware;

import br.com.miz.forcacommiddleware.domain.Game;
import br.com.miz.forcacommiddleware.domain.Player;
import org.springframework.stereotype.Component;
import picocli.CommandLine;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.IntStream;

@Component
@CommandLine.Command(name = "forca")
public class ForcaCommand implements Callable<Integer> {
    private int option;
    private String name = "World";
    private Game game = new Game();
    // Prevent "Unknown option" error when users use
    // the Spring Boot parameter 'spring.config.location' to specify
    // an alternative location for the application.properties file.
    @CommandLine.Option(names = "--spring.config.location", hidden = true)
    private String springConfigLocation;

    @CommandLine.Option(names = { "-j", "--jogadores" }, description = "Número de jogadores", required = true)
    private Integer nPlayers;

    @CommandLine.Option(names = { "-h", "--help" }, usageHelp = true, description = "display a help message")
    private boolean flag;

    @Override
    public Integer call() throws Exception {
        // Business logic here
        Menu menu = new Menu()
                .addOption("Iniciar Jogo")
                .addOption(9, "Sair");
        while (option != 9) {
            System.out.println(menu);
            option = Integer.parseInt(System.console().readLine());
            switch (option) {
                case 1:
                    startGame();
                    break;
                case 9:
                    System.out.println("Encerrando aplicação");
                    break;
                default:
                    System.out.println("Opção inválida!");
            }
        }

        return 0;
    }

    private void startGame() {
        this.game = new Game();
        createPlayer(nPlayers);
        while (!this.game.canIGuess()) {
            System.out.println(game.wordsToPrint());
            askPlayerToPlay();
        }
        System.out.println(game.wordsToPrint());
        tryGuess();
    }

    private void askPlayerToPlay() {
        // Pega o jogador
        Player currentPlayer = this.game.getCurrentPlayer();
        System.out.println(String.format("É a vez do(a) jogador(a) %s (Pontos acumulados: [%d]) jogar:", currentPlayer.getName(), game.getPlayerScore(currentPlayer)));
        // Gira a roleta
        System.out.println("Girando a roleta");
        game.spinRoulette();
        int score = this.game.generateScore();
        if (currentPlayer.equals(game.getCurrentPlayer())) {
            System.out.println(String.format("Valendo %d pontos, escolha uma letra:", score));
            boolean scored = game.addCharacters(System.console().readLine());
            if (scored) {
                System.out.println("Acertou! Joga novamente");
            } else {
                System.out.println(String.format("Errrrroooouuu! Perde a vez. A vez passa para %s",
                        game.getNextPlayer().getName()));
            }
        } else {
            System.out.println(String.format("Que pena! Saiu PERDEU na roleta. A vez passa para %s",
                    game.getCurrentPlayer().getName()));
        }
    }

    private void tryGuess() {
        // Pega o jogador
        Player currentPlayer = this.game.getCurrentPlayer();
        System.out.println("Agora é a hora de tentar adivinhar");
        boolean guessedWordsCorrect = false;
        while (!guessedWordsCorrect) {
            List<String> guessedWords = new ArrayList<>();
            for (int i = 1; i < 4; i++) {
                System.out.println(String.format("%s, qual a %dª palavra?", currentPlayer.getName(), i));
                guessedWords.add(System.console().readLine());
            }
            guessedWordsCorrect = game.guessedWordsCorrect(guessedWords);
            if (guessedWordsCorrect) {
                System.out.println("Acertou! PARABÉNS! Fim de jogo");
            } else {
                currentPlayer = game.getNextPlayer();
                System.out.println(String.format("Errou! Agora é a ver do(a) jogador(a) %s tentar adivinhar!", currentPlayer.getName()));
            }
        }

    }

    private void createPlayer(Integer nPlayers) {

        IntStream.rangeClosed(1, nPlayers).forEach(i -> {
            System.out.println(String.format("Digite o nome do %sº jogador", i));
            this.game.addPlayer(new Player(System.console().readLine()));
        });
    }
}
