package br.com.miz.forcacommiddleware;

import br.com.miz.forcacommiddleware.domain.Game;
import br.com.miz.forcacommiddleware.domain.Player;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class UnitTests {
    @Test
    void menu() {
        Menu menu = new Menu();
        menu.addOption("Primeira")
                .addOption("Segunda")
                .addOption("Terceira");
        System.out.println(menu);
        assertEquals(menu.toString(), "##### Menu ####\n" +
                "1 - Primeira\n" +
                "2 - Segunda\n" +
                "3 - Terceira\n" +
                "Digite uma das opções: ");

    }

    @Test
    void PrintWords() {
        Game game = new Game();
        game.addPlayer("Bruno");
        game.setWords(Map.of("Frutas", Arrays.asList("Banana", "Morango", "Jaca")));
        assertEquals("<Frutas>\n[]\n______\n_______\n____", game.wordsToPrint());
        game.addCharacters("a");
        assertEquals("<Frutas>\n[A]\n_a_a_a\n___a___\n_a_a", game.wordsToPrint());
        game.addCharacters("j");
        assertEquals("<Frutas>\n[A J]\n_a_a_a\n___a___\nJa_a", game.wordsToPrint());
    }

    @Test
    void getNextPlayer() {
        Game game = new Game();
        game.addPlayer("Bruno").addPlayer("Artur");
        assertEquals("Bruno", game.getNextPlayer().getName());
        assertEquals("Artur", game.getNextPlayer().getName());
        assertEquals("Bruno", game.getNextPlayer().getName());
    }

    @Test
    void guessedWordsCorrect() {
        Game game = new Game();
        game.setWords(Map.of("Frutas", Arrays.asList("Banana", "Morango", "Jaca")));
        assertTrue(game.guessedWordsCorrect(Arrays.asList("Banana", "Morango", "Jaca")));
        assertFalse(game.guessedWordsCorrect(Arrays.asList("Banana", "Morango", "Faca")));
        assertFalse(game.guessedWordsCorrect(Arrays.asList("Banana", "Jaca", "")));
    }

    @Test
    void addCharacterAndCanIGuess() {
        Game game = new Game();
        game.addPlayer("Bruno");
        game.setWords(Map.of("Frutas", Arrays.asList("Banana", "Morango", "Jaca")));
        assertTrue(game.addCharacters("o"));
        assertTrue(game.addCharacters("A"));
        assertFalse(game.addCharacters("w"));

        assertFalse(game.canIGuess());
        for(String letter : "eiou".split("")) {
            game.addCharacters(letter);
        }
        assertTrue(game.canIGuess());
    }

    @Test
    void addCharacterAndSCore() {
        Game game = new Game();
        game.setWords(Map.of("Frutas", Arrays.asList("Banana", "Morango", "Jaca")));
        Player player = game.addPlayer("Bruno").getNextPlayer();
        int score = game.generateScore(100);
        game.addCharacters("a");
        assertEquals(score, game.getPlayerScore(player));
        score += game.generateScore(50);
        game.addCharacters("J");
        assertEquals(score, game.getPlayerScore(player));
        game.addCharacters("w");
        assertEquals(score, game.getPlayerScore(player));


    }
}
